//
//  Screen2Sample.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

class Screen2Sample: WKInterfaceController, WCSessionDelegate {
    
    // MARK: Outlets
    // ---------------------

    // 1. Outlet for the image view
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    
    // 2. Outlet for the label
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    // MARK: Delegate functions
    // ---------------------
    
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
//        guard let image = UIImage(data: messageData) else {
//            return
//        }
        
        let messageBody = message["caterpie.png"] as! UIImage
        pokemonImageView.setImage(messageBody)
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Actions
    @IBAction func startButtonPressed() {
        print("Start button pressed")
        if WCSession.default.isReachable{
            print("Attempting to reach")
//            let userInput = self.nameLabel
            
            
            
//let userInput = self.pokemonA.pokemonA(at: 0) as! pokemonImageView
//            userInput.pokemonA.setImage(UIImage(named: "pikachu.png"))
        }
    }
    
    @IBAction func selectNameButtonPressed() {
        print("select name button pressed")
        
        
        if WCSession.default.isReachable{
         print("Attempting to start the game")
         self.nameLabel.setText("My name is Farnaz")
        WCSession.default.sendMessage(
                    ["name" : "My name is Farnaz"],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                        // @TODO: Put some stuff in here to handle any responses from the PHONE
                        print("Message sent, put something here if u are expecting a reply from the phone")
                        self.nameLabel.setText("Got reply from phone")
                }, errorHandler: { (error) in
                    //@TODO: What do if you get an error
                    print("Error while sending message: \(error)")
                    self.nameLabel.setText("Error sending message")
                })
            }
            else {
                print("Phone is not reachable")
                self.nameLabel.setText("Cannot reach phone")
            }
        }
        
        
        
    }
    


